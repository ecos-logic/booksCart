package ecos.logic.org.bookscart.mapping;

import ecos.logic.org.bookscart.dto.BookResponse;
import ecos.logic.org.bookscart.dto.CartItemResponse;
import ecos.logic.org.bookscart.dto.CartResponse;
import ecos.logic.org.bookscart.dto.UserResponse;
import ecos.logic.org.bookscart.entity.Book;
import ecos.logic.org.bookscart.entity.Cart;
import ecos.logic.org.bookscart.entity.CartItem;
import ecos.logic.org.bookscart.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CartMapping {
    UserResponse fromUserEntityToUserDto(User user);
    @Mapping(source = "user", target = "user")
    @Mapping(source = "id", target = "id")
    CartResponse fromEntityToDto(Cart cart);

    List<CartResponse> fromEntityCollectionToDtoCollection(List<Cart> citizenCollection);
    List<CartItemResponse> fromItemEntityCollectionToItemDtoCollection(List<CartItem> citizenCollection);

    @Mapping(source = "book", target = "book")
    @Mapping(source = "quantity", target = "quantity")
    CartItemResponse fromEntityToDto(CartItem cartItem);

    @Mapping(source = "name", target = "title")
    BookResponse fromBookEntityToBookDto(Book expedient);

}
