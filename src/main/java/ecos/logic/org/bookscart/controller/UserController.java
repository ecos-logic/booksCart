package ecos.logic.org.bookscart.controller;

import ecos.logic.org.bookscart.dto.CartItemRequest;
import ecos.logic.org.bookscart.dto.CartRequest;
import ecos.logic.org.bookscart.dto.CartResponse;
import ecos.logic.org.bookscart.entity.Book;
import ecos.logic.org.bookscart.entity.Cart;
import ecos.logic.org.bookscart.entity.CartItem;
import ecos.logic.org.bookscart.entity.User;
import ecos.logic.org.bookscart.mapping.CartMapping;
import ecos.logic.org.bookscart.repository.BookRepository;
import ecos.logic.org.bookscart.repository.CartItemRepository;
import ecos.logic.org.bookscart.repository.CartRepository;
import ecos.logic.org.bookscart.repository.UserRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static java.text.MessageFormat.format;

@RestController
@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {
    private final CartMapping cartMapping;
    private final CartRepository cartRepository;
    private final CartItemRepository cartItemRepository;
    private final UserRepository userRepository;
    private final BookRepository bookRepository;

    public UserController(CartMapping cartMapping, CartRepository cartRepository, CartItemRepository cartItemRepository, UserRepository userRepository, BookRepository bookRepository) {
        this.cartMapping = cartMapping;
        this.cartRepository = cartRepository;
        this.cartItemRepository = cartItemRepository;
        this.userRepository = userRepository;
        this.bookRepository = bookRepository;
    }

    @PostMapping("/{userId}/carts/")
    @Transactional
    public ResponseEntity<CartResponse> addCart(@PathVariable("userId") Integer userId, @Valid @RequestBody CartRequest cartRequest, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(CartResponse.withMessage(String.join(",", bindingResult.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).toList())));


        Optional<User> optionalUser = this.userRepository.findById(userId);
        if (optionalUser.isEmpty())
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(CartResponse.withMessage(format("The user with the id: {0} does not exists", userId)));

        User user = optionalUser.get();
        Optional<Cart> optionalCart = this.cartRepository.findByUser(user);
        if (optionalCart.isPresent())
            return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(CartResponse.withMessage("The user has already a cart."));


        for (CartItemRequest cartItemRequest : cartRequest.getCartItemRequests()) {
            Integer bookId = cartItemRequest.getBookId();
            Optional<Book> optionalBook = this.bookRepository.findById(bookId);
            if (optionalBook.isEmpty())
                return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(CartResponse.withMessage("The requested book does not exist."));
        }

        Cart cart = new Cart();
        cart.setUser(user);
        for (CartItemRequest cartItemRequest : cartRequest.getCartItemRequests()) {
            CartItem cartItem = new CartItem();
            Book book = this.bookRepository.findById(cartItemRequest.getBookId()).orElseThrow();

            cartItem.setCart(cart);
            cartItem.setQuantity(cartItemRequest.getQuantity());
            cartItem.setBook(book);

            cart.addCartItem(cartItem);
        }

        Cart savedCart = this.cartRepository.save(cart);
        cartItemRepository.saveAll(savedCart.getCartItemList());
        return ResponseEntity.ok().body(cartMapping.fromEntityToDto(savedCart));
    }

}
