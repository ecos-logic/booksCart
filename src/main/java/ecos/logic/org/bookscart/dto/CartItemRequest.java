package ecos.logic.org.bookscart.dto;

import jakarta.validation.constraints.Positive;

@SuppressWarnings("unused")
public class CartItemRequest {
    private Integer bookId;
    @Positive(message = "The quantity must be greater than zero.")
    private Long quantity;

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
