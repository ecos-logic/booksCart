package ecos.logic.org.bookscart.repository;

import ecos.logic.org.bookscart.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Integer> {
}
