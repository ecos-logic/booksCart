package ecos.logic.org.bookscart.dto;

@SuppressWarnings("unused")
public class UserResponse {
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
