INSERT INTO Book (id,name,price) VALUES (1,'In the mouth of madness',10);
INSERT INTO Book (id,name,price) VALUES (2,'The neuromancer',15);
INSERT INTO Book (id,name,price) VALUES (3,'Lords and Ladies',25);
ALTER TABLE Book ALTER COLUMN id RESTART WITH (SELECT MAX(id) + 1 FROM Book);


INSERT INTO THE_USER (id,user_name,password) VALUES (1,'username','p@s$w0rd');
INSERT INTO THE_USER (id,user_name,password) VALUES (2,'user name','pa@s$w0rd');
INSERT INTO THE_USER (id,user_name,password) VALUES (3,'user1name1','apa@s$w0rd');
INSERT INTO THE_USER (id,user_name,password) VALUES (4,'aUser','apa@s$w0/rd');
INSERT INTO THE_USER (id,user_name,password) VALUES (5,'aCredential','ap&a@s$w0rd');
INSERT INTO THE_USER (id,user_name,password) VALUES (6,'theUser','aDpa@s$w0rd');
ALTER TABLE THE_USER ALTER COLUMN id RESTART WITH (SELECT MAX(id) + 1 FROM THE_USER);


INSERT INTO Cart (id,user_id) VALUES (1,3);
INSERT INTO Cart (id,user_id) VALUES (2,1);
INSERT INTO Cart (id,user_id) VALUES (3,2);
ALTER TABLE Cart ALTER COLUMN id RESTART WITH (SELECT MAX(id) + 1 FROM Cart);


INSERT INTO CART_ITEM (cart_id,book_id,quantity) VALUES (1,1,1);
INSERT INTO CART_ITEM (cart_id,book_id,quantity) VALUES (2,2,1);
INSERT INTO CART_ITEM (cart_id,book_id,quantity) VALUES (3,3,1);
ALTER TABLE CART_ITEM ALTER COLUMN id RESTART WITH (SELECT MAX(id) + 1 FROM CART_ITEM);