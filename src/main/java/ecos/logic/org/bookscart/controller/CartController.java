package ecos.logic.org.bookscart.controller;

import ecos.logic.org.bookscart.dto.CartItemRequest;
import ecos.logic.org.bookscart.dto.CartResponse;
import ecos.logic.org.bookscart.entity.Book;
import ecos.logic.org.bookscart.entity.Cart;
import ecos.logic.org.bookscart.entity.CartItem;
import ecos.logic.org.bookscart.mapping.CartMapping;
import ecos.logic.org.bookscart.repository.BookRepository;
import ecos.logic.org.bookscart.repository.CartItemRepository;
import ecos.logic.org.bookscart.repository.CartRepository;
import jakarta.validation.Valid;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/carts", produces = MediaType.APPLICATION_JSON_VALUE)
public class CartController {
    private final CartRepository cartRepository;
    private final CartItemRepository cartItemRepository;
    private final BookRepository bookRepository;
    private final CartMapping mapping;

    public CartController(CartRepository cartRepository, CartItemRepository cartItemRepository, BookRepository bookRepository, CartMapping mapping) {
        this.cartRepository = cartRepository;
        this.cartItemRepository = cartItemRepository;
        this.mapping = mapping;
        this.bookRepository = bookRepository;
    }


    @GetMapping("/")
    public ResponseEntity<List<CartResponse>> list() {
        List<Cart> cartResponses = this.cartRepository.findAll();
        List<CartResponse> cartDtoList = mapping.fromEntityCollectionToDtoCollection(cartResponses);
        return ResponseEntity.ok().body(cartDtoList);
    }

    @PostMapping("/{cartId}/cartItems/")
    public ResponseEntity<CartResponse> addCartItem(@PathVariable("cartId") Integer cartId, @Valid @RequestBody CartItemRequest cartItemRequest, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(CartResponse.withMessage(String.join(",", bindingResult.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).toList())));

        Optional<Cart> optionalCart = this.cartRepository.findById(cartId);
        if (optionalCart.isEmpty())
            return ResponseEntity.notFound().build();

        Cart cart = optionalCart.get();
        Optional<Book> optionalBook = this.bookRepository.findById(cartItemRequest.getBookId());
        if (optionalBook.isEmpty())
            return ResponseEntity.notFound().build();

        Book book = optionalBook.get();
        CartItem cartItem = cart.getOrCreateCartItemBy(book);
        cartItem.setCart(cart);
        cartItem.increaseQuantity(cartItemRequest.getQuantity());


        this.cartItemRepository.save(cartItem);

        return ResponseEntity.ok().body(mapping.fromEntityToDto(cart));
    }
}
