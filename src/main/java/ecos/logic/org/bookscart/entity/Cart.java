package ecos.logic.org.bookscart.entity;

import jakarta.persistence.*;

import java.io.Serializable;
import java.util.*;

@Entity
public class Cart implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    private User user;

    @OneToMany(mappedBy = "cart")
    private List<CartItem> cartItemList = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<CartItem> getCartItemList() {
        return cartItemList.stream().toList();
    }

    public void addCartItem(CartItem cartItem) {
        this.cartItemList.add(cartItem);
    }

    public CartItem getOrCreateCartItemBy(Book book) {
        return this.cartItemList.stream().filter(cartItem -> cartItem.getBook().equals(book)).findFirst().orElseGet((() -> {
            CartItem cartItem = new CartItem();
            cartItem.setBook(book);
            cartItem.setQuantity(0L);
            this.cartItemList.add(cartItem);
            return cartItem;
        }));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cart cart = (Cart) o;

        return Objects.equals(id, cart.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
