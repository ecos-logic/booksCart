package ecos.logic.org.bookscart.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@SuppressWarnings("unused")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CartResponse {
    private Integer id;
    private UserResponse user;
    private List<CartItemResponse> cartItemList;
    private String failureMessage;

    public static CartResponse withMessage(String message) {
        CartResponse response = new CartResponse();
        response.failureMessage = message;
        return response;
    }

    public Integer getId() {
        return id;
    }

    public UserResponse getUser() {
        return user;
    }

    public List<CartItemResponse> getCartItemList() {
        return cartItemList;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setUser(UserResponse user) {
        this.user = user;
    }

    public void setCartItemList(List<CartItemResponse> cartItemList) {
        this.cartItemList = cartItemList;
    }

    public String getFailureMessage() {
        return failureMessage;
    }
}
