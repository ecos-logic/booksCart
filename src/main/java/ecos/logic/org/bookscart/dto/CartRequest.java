package ecos.logic.org.bookscart.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

import java.util.List;

@SuppressWarnings("unused")
public class CartRequest {
    private List<CartItemRequest> cartItemRequests;

    public List<@NotNull @Valid CartItemRequest> getCartItemRequests() {
        return cartItemRequests;
    }

    public void setCartItemRequests(List<CartItemRequest> cartItemRequests) {
        this.cartItemRequests = cartItemRequests;
    }


}
