package ecos.logic.org.bookscart.controller;

import org.springframework.cloud.context.restart.RestartEndpoint;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/application")
public class RestartController {
    private final RestartEndpoint restartEndpoint;

    public RestartController(RestartEndpoint restartEndpoint) {
        this.restartEndpoint = restartEndpoint;
    }

    @DeleteMapping("/")
    public void restart() {
        this.restartEndpoint.restart();
    }
}
