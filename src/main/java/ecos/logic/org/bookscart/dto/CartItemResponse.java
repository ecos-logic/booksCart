package ecos.logic.org.bookscart.dto;

@SuppressWarnings("unused")
public class CartItemResponse {
    private BookResponse book;
    private Long quantity;
    public BookResponse getBook() {
        return book;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setBook(BookResponse book) {
        this.book = book;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
