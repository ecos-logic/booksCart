package ecos.logic.org.bookscart.repository;

import ecos.logic.org.bookscart.entity.Cart;
import ecos.logic.org.bookscart.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CartRepository extends JpaRepository<Cart,Integer> {
    @Query("SELECT cart FROM Cart cart WHERE cart.user = ?1")
    Optional<Cart> findByUser(User user);
}
